#!/usr/bin/env ruby

=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

=begin
 *** Squirrel Slave Protocol BNF ***

A command is 'failed' if there is at least one log message with "e"
severity.

server-ready  := "\004#"
client-reply  := command "\n"
server-reply  := server-reply server-stanza | server-stanza
server-stanza := "\001" log-severity log-message "\n" | reply "\n"

log-severity := "d" | "i" | "w" | "e"
log-message  := /[^\n]+/

command := init-command |
           getvar-command | setvar-command
           list-command | info-command |
           resolve-command | build-command |
           update-index-command | bootstrap-command | buildroot-command |
           clean-command |
           exit-command

qbuild-has-package := "+" | "-"
qbuild-id          := qbuild-name " " qbuild-version
qbuild-name        := /[a-z][a-z-]+/
qbuild-version     := /[0-9a-z.+-]+/
qbuild-list        := qbuild-list qbuild-id "\n" | qbuild-id "\n"
qbuild-version-op  := "<" | "<=" | "=" | ">=" | ">"
qbuild-spec        := qbuild-name | qbuild-name qbuild-version-op qbuild-version
qbuild-specs       := qbuild-specs qbuild-spec | qbuild-spec

use-flags := use-flags " " use-flag | use-flag
use-flag  := /[a-z][a-z0-9]+/

option := "offline"

variable-name  := "cross-compiler" | "processor-count" | "target" | "log-level"
variable-value := /[^ \n]/

preinit-command      := "preinit\n"
preinit-reply        := ""

init-command         := "init\n"
init-reply           := ""

var-type             := "flag" | "option" | "make"

getvar-command       := "getvar " var-type " " variable-name "\n"
getvar-reply         := variable-value

setvar-command       := "setvar " var-type " " variable-name " " variable-value "\n"
setvar-reply         := ""

info-command         := "info " qbuild-name " " qbuild-version "\n"
info-reply           := info-seq
info-seq   := info-seq info-line | info-line
info-line  := info-field ": " info-value "\n"
info-field := "Name" | "Version" | "License" | "Homepage" | "Maintainer" |
              "Source" | "Build-Depends" | "Provides" | "USE-Flags" |
              "Packages" | "Category"
info-value := /[^\n]/

list-command         := "list\n"
list-reply           := list-command-reply list-entry | list-entry
list-entry           := qbuild-has-package " " qbuild-id " " use-flags "\n"

resolve-command      := "resolve " qbuild-specs "\n"
resolve-reply        := qbuild-list

build-command        := "build " qbuild-spec "\n"
build-reply          := ""

update-index-command := "update-index\n"
update-index-reply   := ""

bootstrap-command    := "bootstrap\n"
bootstrap-reply      := ""

rootfs-path          := [^ ]+
rootfs-type          := "tgz" | "tbz2"
rootfs-package       := [a-z-]+
rootfs-packages      := rootfs-package " " rootfs-packages | rootfs-packages

buildroot-command    := "buildroot " rootfs-path " " rootfs-type " " rootfs-packages "\n"
buildroot-reply      := ""

clean-command        := "clean\n"
clean-reply          := ""

exit-command         := "exit\n"
=end

$:.unshift File.dirname(__FILE__)

require 'squirrel/squirrel'
require 'socket'

include Squirrel

if ARGV.size != 1
  puts "Usage: #{__FILE__} <unix socket>"
  exit! 1
end

socket = $stdout = UNIXSocket.new(ARGV[0])

logger = Qbuild.logger = MachineParseableLogger.new
Qbuild.root_dir = Dir.getwd
Builder.silent = true

loop {
  print "\004#"
  packet = socket.gets
  break if packet.nil? # ^D

  cmd, *args = *packet.split
  case cmd
    when 'getvar'
      if args.count != 2
        Qbuild.logger.log "Usage: getvar <type> <name>", :error
        next
      end

      var = case args[0]
        when 'flag'
          case args[1]
            when 'offline'; Qbuild.offline.to_s
            when 'verbose'; Builder.verbose.to_s
            else
              Qbuild.logger.log "Unknown flag `#{args[1]}'", :error
              nil
          end

        when 'option'
          case args[1]
            when 'processor-count'; Qbuild.parallel_threads
            when 'cross-compiler';  Qbuild.target_tuple
            when 'log-level';       Qbuild.logger.loglevel
            else
              Qbuild.logger.log "Unknown option `#{args[1]}'", :error
              nil
          end

        when 'make'
          Qbuild.make_conf[args[1]]

        else
          Qbuild.logger.log "Unknown type `#{args[0]}'", :error
          nil
      end
      puts var

    when 'setvar'
      if args.count != 3
        Qbuild.logger.log "Usage: setvar <type> <name> <value>", :error
        next
      end

      case args[0]
        when 'flag'
          value = !(args[2] == 'false')
          case args[1]
            when 'offline'; Qbuild.offline = value
            when 'verbose'; Builder.verbose = value
            else Qbuild.logger.log "Unknown flag `#{args[1]}'", :error
          end

        when 'option'
          case args[1]
            when 'processor-count'; Qbuild.parallel_threads = args[2]
            when 'cross-compiler';  Qbuild.target_tuple = args[2]
            when 'log-level';       Qbuild.logger.loglevel = args[2]
            else Qbuild.logger.log "Unknown variable `#{args[0]}'", :error
          end

        when 'make'
          Qbuild.make_conf[args[1]] = args[2]

        else Qbuild.logger.log "Unknown type `#{args[0]}'", :error
      end

    when 'init'
      Helper.init(false)

    when 'list'
      Warehouse.fetch_all.each { |qbuild|
        print (if qbuild.packages_exist? then '+' else '-' end)
        puts " #{qbuild.name} #{qbuild.version} #{qbuild.can_use.to_a.join ' '}"
      }

    when 'info'
      begin
        qbuild = Warehouse.fetch(args[0], args[1])
        puts "Name: #{qbuild.name}"
        puts "Version: #{qbuild.version}"
        puts "License: #{qbuild.license}"
        puts "Homepage: #{qbuild.homepage}"
        puts "Maintainer: #{qbuild.maintainer}"
        puts "Source: #{qbuild.source}"
        puts "Build-Depends: #{qbuild.build_depends.join ', '}"
        puts "Provides: #{qbuild.provides.join ', '}"
        puts "USE-Flags: #{qbuild.can_use.to_a.join ', '}"
        puts "Packages: #{qbuild.packages.map{|p| p.name}.join ', '}"
        puts "Category: #{qbuild.category}"
      rescue QbuildNotFoundError => e
        logger.log e.message, :error
      end

    when 'resolve'
      begin
        Helper.select(Helper.resolve(args)).each { |qbuild|
          puts qbuild.spec
        }
      rescue SquirrelError => e
        logger.log e.message, :error
      end

    when 'build'
      begin
        Helper.build [Warehouse.get(args[0])]
      rescue SquirrelError => e
        logger.log e.message, :error
      end

    when 'update-index'
      Helper.update_index

    when 'bootstrap'
      Helper.bootstrap

    when 'buildroot'
      Helper.buildroot(args[0], args[1], args[2..-1])

    when 'clean'
      Helper.clean

    when 'exit'
      break

    else
      logger.log "Unknown command `#{cmd}'", :error
  end
}


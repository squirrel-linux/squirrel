=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  class Builder
    @@verbose = @@silent = @@stop_after_qinstall = false

    def self.execute(qbuild, target, logfile, &block)
      builder = self.new
      builder.instance_eval {
        @qbuild = qbuild
        @target = target
        @log = logfile
      }
      builder.instance_eval(&block)
    end

    def self.verbose=(v)
      @@verbose = v
    end

    def self.verbose
      @@verbose
    end

    def self.silent=(s)
      @@silent = s
    end

    def self.silent
      @@silent
    end

    def self.stop_after_qinstall=(s)
      @@stop_after_qinstall = s
    end

    def self.stop_after_qinstall
      @@stop_after_qinstall
    end

    def qget(uri, dest=nil)
      if dest.nil?
        execute "wget --no-check-certificate -N #{uri}"
      elsif !File.exist?(File.join(_('${CACHE}'), _(dest)))
        execute "wget --no-check-certificate #{uri} -O #{dest}"
      end
      _(dest) || File.basename(_(uri))
    end

    def qautoreconf(*params)
      execute "autoreconf --install #{params.join ' '}"
    end

    def qconfigure(*params)
      prefix = ''
      if destination == 'tools'
        prefix = '${TOOLS}'
      end

      cmd = "./configure --build=#{Qbuild.host_tuple} --host=${HOST_TUPLE}", ("--target=${TARGET_TUPLE}" if @qbuild.compiler?),
              "--prefix=#{prefix}/usr --sysconfdir=#{prefix}/etc --localstatedir=#{prefix}/var",
              "--mandir=#{prefix}/usr/share/man CPPFLAGS='${CPPFLAGS}' CFLAGS='${CFLAGS}' LDFLAGS='${LDFLAGS}' #{params.join ' '}"
      execute _(cmd.join(' ')).gsub(/ +/, ' ') # sub-configure fails sometimes with extra spaces
    end

    def qmake(*params)
      execute "make -j#{Qbuild.parallel_threads} #{params.join ' '}"
    end

    def qpatch(patch, p=1)
      patches = []
      fullpath = "${FILES}/#{patch}"
      if patch =~ /^(.+)\.(gz|bz2)$/
        orig = $1
        comp = { 'gz' => 'gunzip', 'bz2' => 'bunzip2' }[$2]

        execute comp, "<#{fullpath}", ">${TMP}/#{orig}"

        fullpath = "${TMP}/#{orig}"

        patches = [ fullpath ]
      elsif File.directory? _(fullpath)
        patches = Dir[_(File.join(fullpath, '*.patch'))].sort
      else
        patches = [ fullpath ]
      end

      patches.each { |file| execute "patch <#{file} -p #{p} -N" }
    end

    def destination
      @qbuild.destination
    end

    def can_use?(flag)
      @qbuild.can_use.include? flag
    end

    def uses?(flag)
      @qbuild.uses?(flag)
    end

    def enable(flag, var=flag)
      if uses?(flag) then " --enable-#{var} " else " --disable-#{var} " end
    end

    def with(flag, value=nil, var=flag)
      if uses?(flag) then
        if value.nil?
          " --with-#{var} "
        else
          " --with-#{var}='#{value}' "
        end
      else
        " --without-#{var} "
      end
    end

    def y(flag)
      if uses?(flag) then "y" else "n" end
    end

    def can_exec?
      @qbuild.can_exec?
    end

    def method_missing(command, *args)
      execute("#{command} #{args.join(' ')}".strip)
    end

    def _(str)
      return nil if str.nil?
      if @qbuild
        str = @qbuild._(str)
      else
        str = Qbuild.prepare(str)
      end
      str.gsub!("${TARGET}", @target) if @target
      str
    end

    def execute(*args)
      # Just POSIX compliant shell is NOT enough
      cmd = _(args.join ' ')
      execute_with(%w{/bin/bash -c}, cmd)
    end
    alias :e :execute

    def execute_fakeroot(&block)
      raise Exception, "execute_fakeroot requires a block" if block.nil?
      executer = Executer.new(self) { |type, cmd|
        cmdline = %Q{fakeroot -i #{Qbuild.tmp_dir}/fakeroot.db -s #{Qbuild.tmp_dir}/fakeroot.db bash -c}
        case type
          when :execute
            execute_with(cmdline.split, cmd)
          when :run
            run "#{cmdline} '#{cmd}'"
        end
      }
      executer.instance_eval(&block)
    end

    class Executer
      def initialize(builder, &block)
        @builder = builder
        @worker = block
      end

      def execute(*args)
        @worker[:execute, @builder._(args.join ' ')]
      end

      def run(*args)
        @worker[:run, args.join(' ')]
      end

      def method_missing(method, *args)
        if @builder.respond_to? method
          @builder.send(method, *args)
        else
          execute("#{method} #{args.join(' ')}".strip)
        end
      end

      attr_reader :commands
    end

    def execute_with(shell, cmd)
      log = File.open(_(@log), "a")

      popen(*(shell + [cmd])) { |o, i|
        if @@verbose
          $stdout.puts cmd
        elsif !@@silent
          log.puts cmd
          $stdout.print "Working |"
        end

        rv = 0
        while s = o.gets
          log.puts s
          if @@verbose
            $stdout.puts s
          elsif !@@silent
            $stdout.print "\10"
            $stdout.print %w{/ - \\ |}[rv]
            rv = (rv + 1) % 4
          end
          $stdout.flush
        end
        $stdout.print "\r" if !@@silent
      }
      log.close

      if !$?.success?
        raise(BuilderException, "command `#{cmd}' failed (#{$?}), check #{@log} for details")
      end
    end

    def run(cmd)
      system(_(cmd))
      $?.success?
    end

    private
    def popen(*cmd)
      por, pow = IO::pipe
      pir, piw = IO::pipe

      fork {
        STDIN.reopen(pir)
        STDOUT.reopen(pow)
        STDERR.reopen(pow)
        [ por, pow, pir, piw ].each &:close

        exec(*cmd)
      }
      pow.close
      pir.close

      begin
        return yield(por, piw)
      ensure
        por.close if !por.closed?
        piw.close if !piw.closed?
        Process.wait
      end
    end
  end
end

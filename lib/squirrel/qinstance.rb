=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'squirrel/qbuild'
require 'set'

module Squirrel
  class Qinstance
    STEPS = Qbuild::STEPS

    attr_reader :qbuild

    def initialize(qbuild, block, destination)
      @@logger ||= Qbuild.logger
      @qbuild = qbuild

      destination = 'target' if destination.nil? || destination.empty?

      @custom_destinations = []
      @compiler = false
      @destination = destination
      @slot = nil
      @packages = []
      @provides = []
      @build_depends = []
      @build_requires = []
      @keywords = []
      @after = []
      @env_flags = false
      @overrides = {}
      @cppflags, @cflags, @ldflags = [], [], []
      @can_use = Set[]

      self.instance_eval &block

      if !(%w{host target tools nothing} + @custom_destinations).include? @destination
        raise Exception, "unknown destination `#{@destination}' for qbuild #{qbuild.name}"
      end
    end

    # attributes
    %w{name version category}.each { |attr|
      define_method(attr) { @qbuild.send attr }
    }

    def id
      idst = ''
      idst = "[#{destination}]" if !%w{target nothing}.include? destination
      "#{category}/#{name}#{idst}-#{version}"
    end

    def spec(dest=destination)
      "#{name}[#{dest}]=#{version}"
    end

    def build_id
      Warehouse.build_ids[self]
    end

    attr_reader :packages

    # destination
    attr_reader :destination

    def target_tuple
      if destination == 'host'
        Qbuild.host_tuple
      else
        Qbuild.target_tuple
      end
    end

    def host_tuple
      if [ 'host', 'tools' ].include? destination
        Qbuild.host_tuple
      else
        Qbuild.target_tuple
      end
    end

    %w{host target}.each { |t|
      define_method("#{t}_arch") do
        send("#{t}_tuple").split('-')[0]
      end
    }

    def can_exec?
      %w{nothing host tools}.include?(destination) || host_arch == Qbuild.host_arch
    end

    def prepare_env
      Qbuild.prepare_env
      ENV['PKG_CONFIG_SYSROOT_DIR'] = "#{staging_dir}"
      ENV['PKG_CONFIG_LIBDIR'] = "#{staging_dir}/usr/lib/pkgconfig"
      if @env_flags
        ENV['CFLAGS'] = cflags
        ENV['CPPFLAGS'] = cppflags
        ENV['LDFLAGS'] = ldflags
      else
        ENV.delete 'CFLAGS'
        ENV.delete 'CPPFLAGS'
        ENV.delete 'LDFLAGS'
      end
    end

    # DSL methods/attributes
    def self.make_dsl(var, default=nil, count=1)
      define_method(var) { |*args|
        if args.size == 0
          instance_variable_get("@#{var}") || default
        elsif args.size == count
          if !instance_variable_get("@#{var}").nil?
            raise QbuildSyntaxError.new("#{var} is already set", @qbuild)
          end
          if block_given?
            instance_variable_set("@#{var}", yield(*args))
          elsif count == 1
            instance_variable_set("@#{var}", args[0])
          else
            raise Exception, "make_dsl called with count != 1 and without block"
          end
        else
          raise QbuildSyntaxError.new("#{var} requries #{count} argument(s)", @qbuild)
        end
      }
    end

    make_dsl(:slot, '0')

    make_dsl(:homepage)
    make_dsl(:license)
    make_dsl(:maintainer, nil, 2) { |name, email| "#{name} <#{email}>" }
    make_dsl(:source) { |uri| Qbuild.prepare(uri) }

    attr_reader :overrides
    def override(file, attrs={})
      raise QbuildSyntaxError, "invalid override syntax" if Set.new(attrs.keys) != Set[ :as, :or ]
      @overrides[file] = attrs
    end

    make_dsl :cppflags
    make_dsl :cflags
    make_dsl :ldflags

    def env_flags
      @env_flags = true
    end

    def compiler;  @compiler = true; end
    def compiler?; @compiler; end

    def no_unpack_depends; @no_unpack_depends = true; end

    def can_make(*args)
      @custom_destinations += args.map { |d| d.to_s }
    end

    attr_reader :custom_destinations

    def provides(*args)
      if args.size >= 1
        @provides += args.map &:to_s
      else
        @provides.clone
      end
    end

    def build_depends(*args)
      if args.size >= 1
        @build_depends += args
      else
        @build_depends.clone
      end
    end

    def build_requires(*args)
      if args.size >= 1
        @build_requires += args
      else
        @build_requires.clone
      end
    end

    def after(*args)
      if args.size >= 1
        @after += args
      else
        @after.clone
      end
    end

    def can_use(*flags)
      if flags.size
        @can_use.merge flags
      else
        @can_use.clone
      end
    end

    def keywords(*args)
      if args.size >= 1
        @keywords += args
      else
        Set.new(@keywords)
      end
    end

    # use flags
    def override_use(new_use)
      @override_use = new_use
    end

    def uses?(flag)
      @@logger.log "Check for use flag `#{flag}' not specified by package #{self.id}", :warn if !can_use.include? flag
      Warehouse.use_flags(self).include? flag
    end

    def used
      Warehouse.use_flags(self) & can_use
    end

    def unused
      can_use - used
    end

    # dependencies
    def qbuild_depends
      propagate_dest = if @custom_destinations.include? destination then 'target' else destination end
      (@qbuild_depends ||= @build_depends.map { |bd| Warehouse.tweak_spec(bd, propagate_dest) }.
                                          map { |bd| Warehouse.get(bd) })
    end

    def all_qbuild_depends(depends=[])
      root = depends.empty?
      return depends if depends.include?(self)
      depends << self
      depends + qbuild_depends.map { |d| d.all_qbuild_depends(depends) }.reduce([]) { |a,v| a + v } if !@no_unpack_depends
      depends.uniq!
      depends.reject! { |qb| qb == self } if root
      depends
    end

    def weak?
      @weak || false
    end

    def strong?
      @weak == false
    end

    def weaken
      @weak = true
    end

    def strengthen
      @weak = false
    end

    def packages_exist?
      if ![ 'host', 'target' ].include?(destination) && (destination == 'tools' || @packages.empty?)
        Warehouse.tools.built? self
      else
        @packages.map { |p| p.exists? }.reduce(true) { |p,v| p && v }
      end
    end

    def needs_building?
      !(weak? && packages_exist?)
    end

    Qbuild::DIRS.each { |dir|
      method = "#{dir}_dir"
      define_method(method) { @qbuild.send(method) }
    }

    def staging_dir
      if destination == 'tools'
        Qbuild.tools_dir
      elsif destination == 'host'
        Qbuild.host_staging_dir
      else
        Qbuild.staging_dir
      end
    end

    def host_staging_dir
      Qbuild.host_staging_dir
    end

    def source_dir
      "#{root_dir}/src_#{name}-#{version}"
    end

    def pack_dir
      "#{root_dir}/pack_#{name}-#{version}"
    end

    def trampoline_dir
      "#{root_dir}/tramp_#{name}-#{version}"
    end

    def _(path)
      replaces = {
        "SRC_URI"      => source || '',
        "P"            => "#{name}-#{version}",
        "V"            => version,
        "STAGING"      => staging_dir,
        "SRC"          => source_dir,
        "PKG"          => pack_dir,
        "TRAMP"        => trampoline_dir,
        "CPPFLAGS"     => cppflags,
        "CFLAGS"       => cflags,
        "LDFLAGS"      => ldflags,
        "CROSS"        => "#{host_tuple}-",
        "HOST_TUPLE"   => host_tuple,
        "HOST_ARCH"    => host_arch,
        "TARGET_TUPLE" => target_tuple,
        "TARGET_ARCH"  => target_arch,
      }

      old_path = path.clone
      loop {
        path = @qbuild.prepare(path)
        replaces.each { |var, value|
          path.gsub! "${#{var}}", value
        }
        break if path == old_path
        old_path = path.clone
      }

      path
    end

    # flags
    def cppflags(*args)
      if args.size > 0
        @cppflags += args
      else
        local_cppflags = []
        local_cppflags << "-I#{staging_dir}/usr/include" if @build_depends.any?
        local_cppflags += @cppflags
        local_cppflags << (Qbuild.make_conf['CPPFLAGS'] || '') if !%w{host target}.include? destination
        local_cppflags.join(' ')
      end
    end

    def cflags(*args)
      if args.size > 0
        @cflags += args
      else
        local_cflags = []
        local_cflags += @cflags
        local_cflags << (Qbuild.make_conf['CFLAGS'] || '') if !%w{host target}.include? destination
        local_cflags.join(' ')
      end
    end

    def ldflags(*args)
      if args.size > 0
        @ldflags += args
      else
        local_ldflags = []
        if @build_depends.any?
          local_ldflags << "-L#{staging_dir}/lib" <<
                           "-L#{staging_dir}/usr/lib"
          local_ldflags << "-Wl,-rpath-link,#{staging_dir}/lib" <<
                           "-Wl,-rpath-link,#{staging_dir}/usr/lib" if destination == 'target'
        end
        local_ldflags += @ldflags
        local_ldflags << (Qbuild.make_conf['LDFLAGS'] || '') if !%w{host target}.include? destination
        local_ldflags.join(' ')
      end
    end

    # state
    def _state=(step)
      File.open(_("${SRC}/.squirrel.state"), "w") { |f|
        f.write step.to_s
      }
    end

    def _state
      begin
        File.read(_("${SRC}/.squirrel.state")).to_sym
      rescue
        STEPS[0]
      end
    end

    # actions
    def _action(action, block)
      if block.nil?
        yield instance_variable_get("@_#{action}")
      else
        instance_variable_set("@_#{action}", block)
      end
    end

    def has_action?(action)
      !instance_variable_get("@_#{action}").nil?
    end

    Qbuild::ACTIONS.each { |action|
      define_method(action + '!') { |*args, &block|
        self.send(action.intern, *args, &block) if !has_action?(action)
        self.send(action.intern, *args)
      }
      protected (action + '!')
    }

    # building
    def _build(start_step=nil, stop_step=nil)
      raise Exception, "cannot build package with `nothing' destination" if @destination == "nothing"

      do_defaults = (Qbuild::DEFAULT_DESTINATIONS.include? @destination)

      steps = STEPS.dup
      if @destination == 'tools'
        [ :unpack_depends, :package, :install_custom ].each { |s| steps.delete s }
      else
        steps.delete :install_tools
        steps.delete :install_custom if do_defaults || !has_action?(:install_custom)
        steps.delete :unpack_depends if @no_unpack_depends
      end

      start_step ||= steps.first
      stop_step ||= steps.last

      raise Exception, "invalid start step #{start_step}" if !steps.include? start_step.to_sym
      start_step = steps.index(start_step.to_sym)

      raise Exception, "invalid stop step #{stop_step}" if !steps.include? stop_step.to_sym
      stop_step = steps.index(stop_step.to_sym)

      prepare_env

      start_step.upto(stop_step) { |step_index|
        step = steps[step_index]
        case step
          when :checkout
            FileUtils.rm_rf source_dir
            FileUtils.mkdir_p source_dir
            FileUtils.mkdir_p cache_dir

            dest = ''
            dest = "[#{destination}]" if destination != 'target'
            @@logger.log "Building package \33[1;36m#{id}\33[0m."

            if has_action? :checkout
              @@logger.log "Checking out sources..."
              checkout
            else
              archive = nil
              if !Qbuild.offline
                @@logger.log "Downloading sources..."
                archive = download! {
                  qget "${SRC_URI}"
                }
              else
                archive = _(File.basename source)
                raise BuilderException, "cannot find cached sources" if !File.exists? "#{cache_dir}/#{archive}"
              end
              @@logger.log "Unpacking sources..."
              unpack!(archive) {
                if _("${TARGET}") =~ /\.(tar\.|t)(gz|bz2|xz)$/
                  tar "xf ${CACHE}/${TARGET} --strip=1"
                else
                  raise BuilderException, "unknown archive format"
                end
              }
            end

          when :prepare
            if has_action? :prepare
              @@logger.log "Preparing build environment..."
              prepare
            end

          when :unpack_depends
            if qbuild_depends.any?
              @@logger.log "Unpacking build dependencies..."

              [ staging_dir, host_staging_dir ].each { |dir|
                FileUtils.rm_rf dir
                FileUtils.mkdir_p dir
              }

              all_qbuild_depends.select { |qb| %w{host target headers compiler}.include? qb.destination }.
                                 each { |qb|
                qb.packages.each { |pkg|
                  staging = if qb.destination == 'host' then host_staging_dir else staging_dir end

                  Builder.execute(self, "#{root_dir}/dist", "${SRC}/depends_#{pkg.name}.log") {
                    execute "cd ${TMP} && ar x ${TARGET}/#{pkg.filename}"
                    tar "xvf", "${TMP}/data.tar.gz", "-C #{staging}"
                  }
                }
              }

              if destination != 'tools'
                Dir["#{staging_dir}/usr/lib/*.la"].each { |la|
                  contents = File.read(la)
                  contents.gsub! /([' ])\/usr/, "\\1#{staging_dir}/usr"
                  File.open(la, "w") { |fd| fd.write contents }
                }

                Dir["#{staging_dir}/lib/*.la"].each { |la|
                  contents = File.read(la)
                  contents.gsub! /([' ])\/([\/l])/, "\\1#{staging_dir}/\\2"
                  File.open(la, "w") { |fd| fd.write contents }
                }
              end
            end

          when :configure
            _do_overrides
            @@logger.log "Configuring..."
            configure! {
              qconfigure if do_defaults
            }

            %x{find #{source_dir} -type f -name configure}.split.each { |configure|
              [ 'libtool', 'config.lt' ].each { |config|
                libtool = File.join(File.dirname(configure), config)
                if File.exists? libtool
                  contents = File.read(libtool)
                  contents.gsub! /^(hardcode_automatic=)(.*)$/, "\\1yes"
                  contents.gsub! /^(hardcode_into_libs=)(.*)$/, "\\1no"
                  contents.gsub! /^(link_all_deplibs=)(.*)$/, "\\1yes"
                  File.open(libtool, "w") { |fd| fd.write contents }
                end
              }
            }

          when :build
            @@logger.log "Building..."
            build! {
              qmake if do_defaults
            }

          when :package
            if !build_id.nil?
              if @packages.map { |p| p.exists? }.include? true
                @@logger.log "Removing obsolete packages..."

                @packages.each { |package|
                  FileUtils.rm_f File.join(dist_dir, package.filename)
                }

                Warehouse.build_ids.advance self
                @@logger.log "Advanced to build #{build_id}."
              end
            end

            @packages.each { |package|
              @@logger.log "Creating package \33[1;33m#{package.name}\33[0m..."
              package.build(pack_dir, dist_dir, trampoline_dir)
              FileUtils.rm_f "#{Qbuild.tmp_dir}/fakeroot.db"
            }

            if build_id.nil?
              Warehouse.build_ids.advance self
            end

          when :install_tools
            @@logger.log "Installing tools..."
            install_tools! {
              qmake 'install'
            }
            %w{aclocal doc man info locale}.each { |dir|
              FileUtils.rm_rf "#{tools_dir}/usr/share/#{dir}"
            }
            Warehouse.tools.register self

          when :install_custom
            @@logger.log "Performing qbuild installation..."
            install_custom
            Warehouse.tools.register self

          when :finish
            # don't use staging_dir to not accidentally delete tools_dir
            FileUtils.rm_rf Qbuild.staging_dir
            FileUtils.rm_rf Qbuild.host_staging_dir
            FileUtils.rm_rf source_dir
            FileUtils.rm_f "#{Qbuild.tmp_dir}/fakeroot.db"

            @@logger.log "Finished."
        end

        self._state = STEPS[step_index+1] if step != :finish
      }
    end

    def _tune
      _build(_state, :prepare)
      _do_overrides
      @@logger.log "Starting qbuild configuration..."
      tune! {
        raise BuilderException, "No tune action was defined by qbuild"
      }
      _update_overrides
      @@logger.log "Done."
    end

    # API
    protected

    %w{unpack checkout prepare build install_tools install_custom}.each { |action|
      define_method(action) { |&block|
        _action(action, block) { |block|
          Dir.chdir(source_dir)
          Builder.execute(self, source_dir, "${SRC}/#{action}.log", &block)
        }
      }
    }

    def download(&block)
      _action(:download, block) { |block|
        Dir.chdir(cache_dir)
        Builder.execute(self, cache_dir, "${SRC}/download.log", &block)
      }
    end

    def unpack(archive=nil, &block)
      _action(:unpack, block) { |block|
        Dir.chdir(source_dir)
        Builder.execute(self, archive, "${SRC}/unpack.log", &block)
      }
    end

    %w{tune configure}.each { |action|
      define_method(action) { |&block|
        _action(action.intern, block) { |block|
          Dir.chdir(source_dir)
          Builder.execute(self, source_dir, "${SRC}/#{action}.log", &block)
        }
      }
    }

    def package(name, description, section, priority, &block)
      @packages << Package.new(self, name, description, section, priority, &block)
    end

    # overrides
    def _do_overrides
      return if @overrides.empty?

      @@logger.log "Overriding configuration..."
      @overrides.each { |file, attrs|
        target = _"${SRC}/#{file}"
        override = _"${ETC}/override/#{attrs[:as]}"
        default = _"${FILES}/#{attrs[:or]}"

        if File.exists? override
          FileUtils.cp override, target
        else
          FileUtils.cp default, target
        end
      }
    end

    def _update_overrides
      return if @overrides.empty?

      @@logger.log "Updating configuration overrides..."
      @overrides.each { |file, attrs|
        target = _"${SRC}/#{file}"
        override = _"${ETC}/override/#{attrs[:as]}"

        if File.exists? target
          if !File.exists?(override) || File.read(override) != File.read(target)
            FileUtils.mkdir_p File.dirname(override)
            FileUtils.cp target, override
          else
            @@logger.log "Override `#{file}' has not changed."
          end
        else
          @@logger.log "Override `#{file}' does not exist.", :warn
        end
      }
    end
  end
end

=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'fileutils'
require 'squirrel/exceptions'

module Squirrel
  class Package
    def initialize(qbuild, name, description, section, priority, &block)
      if !%w{admin base comm editors extras graphics games libs runtime misc net text web utils x11}.include?(section.to_s)
        raise Exception, "bad section name `#{section}'"
      end

      if !%w{required standard important optional extra}.include?(priority.to_s)
        raise Exception, "bad priority `#{priority}'"
      end

      @qbuild = qbuild
      @name, @description = name, description
      @section, @priority = section, priority
      @block = block
    end

    attr_reader :name, :description, :section, :priority

    def build(pack_dir, dest_dir, trampoline_dir)
      FileUtils.rm_rf pack_dir
      FileUtils.rm_rf trampoline_dir
      FileUtils.mkdir_p pack_dir
      FileUtils.mkdir_p trampoline_dir

      target_dir = File.join(@qbuild.dist_dir, File.dirname(filename))
      FileUtils.mkdir_p target_dir

      control_file = <<EOC
Package: #{@name}
Version: #{local_version}
Priority: #{@priority}
Architecture: #{@qbuild.host_arch}
Section: #{@section}
Source: #{@qbuild.source}
Maintainer: #{@qbuild.maintainer}
Description: #{@description}
X-Category: #{@qbuild.category}
EOC

      package_block = @block
      qbuild = @qbuild
      PackageBuilder.execute(qbuild, File.join(qbuild.dist_dir, filename),
                             "${SRC}/package_#{@name}.log", @name) {
        FileUtils.mkdir_p "#{pack_dir}/CONTROL"
        File.open("#{pack_dir}/CONTROL/control", "w") { |f|
          f.write _(control_file)
        }

        Dir.chdir qbuild.source_dir
        instance_eval &package_block
      }
    rescue StopAfterQinstallException
    end

    def filename
      if @qbuild.destination == 'host' then 'host/' else '' end +
          "#{@name}_#{local_version}_#{@qbuild.host_arch}.ipk"
    end

    def exists?
      File.exists?(File.join(@qbuild.dist_dir, filename))
    end

    def local_version
      build_id = ''
      build_id = "+build#{@qbuild.build_id}" if @qbuild.build_id && @qbuild.build_id > 0
      "#{@qbuild.version}#{build_id}"
    end
  end

  class PackageBuilder < Builder
    def self.execute(qbuild, target_dir, logfile, package, &block)
      super(qbuild, target_dir, logfile) {
        @relations = {}

        execute_fakeroot {
          instance_eval &block
        }

        rm "-rf ${PKG}/usr/share/{doc,man,info}" if can_use?(:doc) && !uses?(:doc)
        if can_use?(:locale) && !uses?(:locale)
          rm "-rf ${PKG}/usr/share/{locale,aclocal}"
          Dir[_"${PKG}/usr/share/man/*"].each { |mandir|
            rm "-rf #{mandir}" if mandir !~ /\/man[0-9]$/
          }
        end
        @relations.each do |field, args|
          execute "echo '#{field}: #{args.join ', '}' >>${PKG}/CONTROL/control"
        end
        %w{preinst postinst prerm postrm}.each { |script|
          send(script, "#{package}.#{script}") if File.exists? _("${FILES}/#{package}.#{script}")
        }

        FileUtils.mkdir_p _("${TMP}/pkg")
        execute_fakeroot {
          execute "cd ${PKG}/CONTROL && tar -czf ${TMP}/pkg/control.tar.gz ."
          execute "cd ${PKG} && tar --exclude=CONTROL -czf ${TMP}/pkg/data.tar.gz ."
          execute "echo 2.0 >${TMP}/pkg/debian-binary"
          execute "ar crf ${TARGET} ${TMP}/pkg/debian-binary ${TMP}/pkg/control.tar.gz ${TMP}/pkg/data.tar.gz"
        }
        rm "-rf ${PKG} ${TRAMP}"
      }
    end

    def qinstall_generic(action, *params)
      params.unshift 'DESTDIR=${TRAMP}' if destination != 'tools'
      execute "make #{action} #{params.join ' '}"
      raise StopAfterQinstallException if Builder.stop_after_qinstall
    end
    private :qinstall_generic

    def qinstall(*params)
      qinstall_generic('install-strip', params)
    end

    def qinstall_dostrip(*params)
      qinstall_generic('install', params)
      if @qbuild.destination == 'target'
        cmd = _"find ${TRAMP} -print0 2>&1 | xargs -0 file | grep -F ELF | cut -d ':' -f 1"
        `#{cmd}`.split.each { |elf|
          e "chmod u+w #{elf} && ${CROSS}strip #{elf}"
        }
      end
    end

    def qdist(*paths)
      raise Exception, "qdist called without arguments" if paths.empty?
      paths.each { |p|
        weak = (p[0] == ?@)
        p = p[1..-1] if weak

        expanded = Dir[_"${TRAMP}/#{p}"].map{ |s| s.sub _("${TRAMP}/"), '' }
        if expanded.empty?
          raise BuilderException, "#{p} does not exists" if !weak
          next
        end
        expanded.each { |e|
          FileUtils.mkdir_p _("${PKG}/#{File.dirname e}")
          execute "mv ${TRAMP}/#{e} ${PKG}/#{e}"
        }
      }
    end

    def qdist_app(name='*')
      qdist "usr/bin/#{name}"
    end

    def qdist_lib(name='*')
      qdist "usr/lib/lib#{name}.so*"
    end

    def qdist_lib_dev(name='*')
      if name == '*'
        qdist "usr/include"
      else
        qdist "@usr/include/#{name}.h"
      end
      qdist "@usr/lib/lib#{name}.a"
      qdist "@usr/lib/pkgconfig/#{name}.pc", "@usr/lib/lib#{name}.la"
    end

    def qdist_doc
      qdist "@usr/share/doc", "@usr/share/man", "@usr/share/info"
    end

    def qdist_locale
      qdist "@usr/share/locale", "@usr/share/aclocal"
    end

    RELATIONS = %w{Depends Pre-Depends Recommends Suggests Enhances Provides Conflicts Tags}
    RELATIONS.each { |field|
      define_method(field.downcase.gsub '-', '_') do |*args|
        @relations[field] ||= []
        @relations[field] += args
      end
    }

    %w{preinst postinst prerm postrm}.each { |script|
      define_method(script) do |filename|
        execute "cp ${FILES}/#{filename} ${PKG}/CONTROL/#{script}"
        execute "chmod +x ${PKG}/CONTROL/#{script}"
      end
    }

    def conffiles(*files)
      File.open(_("${PKG}/CONTROL/conffiles"), "a") { |f|
        f.write files.map { |f| f + "\n" }.join
      }
    end
    alias :conffile :conffiles
  end
end

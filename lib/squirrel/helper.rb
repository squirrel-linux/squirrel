=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'squirrel/qbuild'
require 'squirrel/logger'

module Squirrel
  module Helper
    def self.preinit
      begin
        load "#{Qbuild.etc_dir}/squirrel.conf"
        Qbuild.target_tuple = CROSS_COMPILER
        if defined?(:THREADS)
          Qbuild.parallel_threads = THREADS
        else
          Qbuild.parallel_threads = 1
        end
        true
      rescue LoadError
        Qbuild.logger.log "etc/squirrel.conf does not exist or has bad syntax", :error
        false
      rescue
        Qbuild.logger.log "etc/squirrel.conf does not define CROSS_COMPILER", :error
        false
      end
    end

    def self.init(load_configs=true)
      Qbuild.prepare_env
      Qbuild.load_config if load_configs
      FileUtils.mkdir_p "#{Qbuild.root_dir}/tmp/"

      begin
        Warehouse.init

        true
      rescue QbuildSyntaxError => e
        Qbuild.logger.log "Syntax error in #{File.basename e.filename}: #{e.message}", :error

        false
      rescue SquirrelError => e
        Qbuild.logger.log e.message, :error

        false
      end
    end

    def self.resolve(specs, keep_built=false, force_build=false, options={})
      Qbuild.logger.log "Resolving packages..."

      specs.map! { |spec| if spec == 'world' then Warehouse.world.specs else spec end }
      specs.flatten!

      r = Resolver.new(specs, options)
      if r.auto_resolve
        Warehouse.resolver = r
        r.qbuilds.each { |q| q.weaken } if !force_build
        specs.each     { |s| r.resolved_specs[s].strengthen } if !keep_built

        r.qbuilds
      else
        nil
      end
    end

    def self.select(qbuilds)
      inverse_depends = {}
      ordered_qbuilds = []

      qbuilds.each { |qb|
        qb.qbuild_depends.each { |dep|
          inverse_depends[dep] = qb
        }
      }

      unresolved_before = nil
      while ordered_qbuilds.size < qbuilds.size
        unresolved = (qbuilds - ordered_qbuilds)
        if unresolved_before == unresolved
          Qbuild.logger.log "Cannot decide build order. Offending qbuilds: #{unresolved.map{|q|q.id}.join ', '}.", :error
          return []
        end

        unresolved_before = unresolved
        unresolved.each { |qb|
          next if ((qb.after.map { |aq| Warehouse.resolver.resolved_specs[aq] }.
                             compact - ordered_qbuilds) & qbuilds).any?
          if (qb.qbuild_depends - ordered_qbuilds).empty?
            ordered_qbuilds << qb
          end
        }
      end

      ordered_qbuilds.select{ |q| q.needs_building? }
    end

    # returns *failed* packets or true
    def self.build(qbuilds, specs=[], options={})
      failures = []
      skip = []
      qbuilds.each { |qbuild|
        next if skip.include? qbuild # don't remove qbuilds while iterating!
        begin
          start = nil
          start = qbuild._state if options[:try_resume]
          qbuild._build(start, options[:stop_after])
          next if options[:stop_after]

          Warehouse.state.append qbuild
          specs.each { |spec|
            if Warehouse.resolver.resolved_specs[spec] == qbuild
              Warehouse.world.append spec
            end
          }
        rescue BuilderException => e
          if options[:ignore_errors]
            Qbuild.logger.log(e.message, :warn)
            Qbuild.logger.log("Building #{qbuild.id} was unsuccessful, removing dependants...", :warn)
            dependants = qbuilds.select { |q| q.all_qbuild_depends.include? qbuild }
            skip += dependants
            Qbuild.logger.log("Removed: #{dependants.map { |d| "#{d.id}" }.join ', '}", :warn) if dependants.any?
            Qbuild.logger.log("Continuing build.", :warn)
            failures << qbuild
          else
            Qbuild.logger.log(e.message, :error)
            return [ qbuild ]
          end
        end
      }
      (options[:ignore_errors] && failures) || true
    end

    def self.update_index
      begin
        Qbuild.logger.log "Generating package index..."

        Dir.chdir Qbuild.root_dir
        Builder.execute(nil, "#{Qbuild.dist_dir}", "${TMP}/pkg-make-index.log") {
          execute <<EOMI
for pkg in ${TARGET}/*.ipk; do
	file_size=$(ls -l $pkg | awk '{print $5}')
	md5sum=$(md5sum $pkg | awk '{print $1}')
	ar -pf $pkg ./control.tar.gz | tar xzOf - ./control | sed -e "s/^Description:/Filename: `basename $pkg`\\nSize: $file_size\\nMD5Sum: $md5sum\\nDescription:/"
	echo
done >${TARGET}/Packages
EOMI
        }
      rescue BuilderException => e
        Qbuild.logger.log(e.message, :error)
      end
    end

    def self.bootstrap(force=false)
      Qbuild.logger.log "Bootstrapping squirrel..."

      begin
        qbuilds = resolve(%w{opkg[tools] pkg-config[tools] fakeroot[tools] squirrel-quirks[tools]}, true, force)
        qbuilds = select(qbuilds)
        qbuilds.each { |qbuild|
          qbuild.override_use []
          qbuild._build if qbuild.needs_building?
        }

        true
      rescue SquirrelError => e
        Qbuild.logger.log(e.message, :error)

        false
      end
    end

    ROOTFS_TYPES = %w{tgz tbz2 cpiogz}

    def self.build_rootfs(target, type, packages, bare=false)
      if !ROOTFS_TYPES.include? type
        Qbuild.logger.log "Unknown rootfs type #{type}.", :error
        return false
      end

      packages.unshift 'fhs-layout' if !bare

      begin
        Qbuild.logger.log "Preparing opkg environment..."

        Dir.chdir Qbuild.root_dir

        buildroot = "#{Qbuild.tmp_dir}/buildroot"

        FileUtils.rm_rf   buildroot
        FileUtils.rm_f    "#{Qbuild.tmp_dir}/fakeroot.db"
        FileUtils.mkdir_p buildroot
        FileUtils.mkdir_p "#{buildroot}/var/lib/opkg"
        FileUtils.mkdir_p "#{buildroot}/etc/opkg"

        File.open("#{buildroot}/etc/opkg/arch.conf", "w") { |f|
          f.puts "arch #{Qbuild.target_arch} 100"
        }
        File.open("#{buildroot}/etc/opkg/bootstrap-feed.conf", "w") { |f|
          f.puts "src bootstrap file:#{Qbuild.dist_dir}"
        }

        Builder.execute(nil, target, "${TMP}/build_rootfs.log") {
          Qbuild.logger.log "Preparing root filesystem..."

          execute_fakeroot {
            execute "opkg -o #{buildroot} update"

            run "opkg -o #{buildroot} install #{packages.join ' '}"

            execute "rm -rf #{buildroot}/etc/opkg #{buildroot}/var/lib/opkg" if bare

            case type.to_s
              when 'tgz', 'tbz2'
                tar_type = { 'tgz' => 'z', 'tbz2' => 'j' }[type]
                execute "cd #{buildroot} && tar c#{tar_type}vf #{target}.#{type} *"
              when 'cpiogz'
                execute "cd #{buildroot} && (find . | cpio -o -H newc | gzip >#{target}.cpio.gz)"
            end
          }
        }

        true
      rescue BuilderException => e
        Qbuild.logger.log(e.message, :error)

        false
      end
    end

    def self.clean
      Qbuild.logger.log "Removing build orphans..."
      FileUtils.rm_rf(Dir["pack_*", "src_*", "tramp_*", "tmp/*", "staging/", "host_staging/"])
      true
    end
  end
end

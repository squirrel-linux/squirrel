=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  class Logger
    LEVELS = [ :error, :warn, :info, :debug ].freeze

    def initialize
      @loglevel = :info
    end

    def log(text, level=:info)
      check_loglevel(level)
      do_log(text, level) if LEVELS.index(level) <= LEVELS.index(@loglevel)
    end

    attr_reader :loglevel

    def loglevel=(new_level)
      new_level = new_level.to_sym
      check_loglevel(new_level)
      @loglevel = new_level
    end

    private

    def check_loglevel(level)
      raise Exception, "Wrong loglevel #{level}" if !LEVELS.include? level
    end
  end

  class HumanReadableLogger < Logger
    TYPES = {
      :info  => [ "32",   'INFO ' ], # green
      :error => [ "1;31", 'ERROR' ], # red
      :warn  => [ "1;33", 'WARN ' ], # yellow
      :debug => [ "37",   'DEBUG' ], # gray
    }

    private
    def do_log(text, type)
      color, message = *TYPES[type]
      line = "\33[0;#{color}m[#{message}]\33[0m #{text}\n"
      line.gsub! /\e\[[0-9;]+m/, '' if !$stdout.tty?
      $stdout.puts line
    end
  end

  class MachineParseableLogger < Logger
    private
    def do_log(text, type)
      $stdout.puts "\001#{type.to_s[0].chr}#{text.gsub /\e\[[0-9;]+m/, ''}"
    end
  end
end

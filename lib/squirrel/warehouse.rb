=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'squirrel/config/files'
require 'squirrel/resolver'
require 'squirrel/exceptions'

module Squirrel
  module Warehouse
    def self.init
      @target_use = UseFlags.new('target')
      @host_use = UseFlags.new('host')
      @tools_use = UseFlags.new('tools')

      @keywords = Keywords.new
      @keyword_mapping = KeywordMapping.new

      @instance_cache = {}

      Qbuild.logger.log("Loading qbuilds...")
      @qbuilds = {}
      @_qinstances = [] # for internal use
      Dir["#{Qbuild.root_dir}/qbuilds/**/*.qbuild"].each { |qbuild|
        Kernel.load qbuild
      }

      [ @target_use, @host_use, @tools_use ].each { |use| use.verify }

      @virtuals = {}
      @_qinstances.each { |q|
        (q.provides || []).each { |p|
          raise QbuildSyntaxError, "Qbuild #{q.spec} provides #{p}, and package with same name exists" if @qbuilds.has_key? p

          @virtuals[p] ||= []
          @virtuals[p] << q.qbuild
        }
      }

      FileUtils.mkdir_p Qbuild.var_dir

      @build_ids = BuildIds.new
      @tools = Tools.new
      @world = World.new
      @state = State.new
    end

    class << self
      %w{world state target_use host_use tools_use keywords build_ids tools}.each { |reader|
        define_method(reader) {
          instance_variable_get("@#{reader}")
        }
      }
    end

    def self.add(qbuild)
      @qbuilds[qbuild.name] ||= {}
      @qbuilds[qbuild.name][qbuild.version] = qbuild
      @_qinstances << qbuild.invoke('nothing')
    end

    def self.slug(qbuild, dest)
      if dest.nil? # partial
        "#{qbuild.name}=#{qbuild.version}"
      else
        "#{qbuild.name}=#{qbuild.version}[#{dest}]"
      end
    end
    private_class_method :slug

    def self.virtuals
      @virtuals
    end

    def self.possible_use_flags
      Set.new(@_qinstances.map { |p| p.can_use }.reduce([]) { |a,v| a + v.to_a })
    end

    def self.use_flags(target=nil, dest_override=nil)
      use = { 'host' => @host_use, 'target' => @target_use, 'tools' => @tools_use }
      if target.nil?
        @target_use
      else
        dest = dest_override || target.destination
        if use.has_key? dest
          use[dest].flags_for(target)
        else
          Set[]
        end
      end
    end

    def self.keyword(target)
      arch = @keyword_mapping.map(Qbuild.target_arch)
      if target.keywords.include? "~#{arch}"
        "~#{arch}"
      else
        false
      end
    end

    def self.uses_packages(flag)
      @_qinstances.select { |p| p.can_use.include? flag }
    end

    def self.flush
      @instance_cache = {}
    end

    def self.fetch(name, version, destination='target')
      qbuilds = @qbuilds[name]
      if qbuilds
        qbuild = qbuilds[version]
        if qbuild
          @instance_cache[slug(qbuild, destination)] ||= qbuild.invoke(destination)
        else
          nil
        end
      else
        nil
      end
    end

    def self.fetch_all
      @_qinstances.map { |q| fetch q.name, q.version }
    end

    # Resolving stuff

    def self.resolver=(r)
      raise Exception, "trying to assign unfinished resolver to Warehouse" if !r.resolved?
      @resolver = r
    end

    def self.resolver
      @resolver
    end

    def self.dissect_spec(spec)
      spec.match(/^([a-z-]+?\/)?([a-z0-9-]+)(\[([a-z]+)\])?((>=?|=|<=?)?([0-9a-z.+~*-]+))?(\{(([+-][a-z0-9-]+[, ]?)+)\})?$/).to_a
    end
    private_class_method :dissect_spec

    def self.parse_spec(spec, default_dest=nil)
      a = dissect_spec(spec)
      { :section => a[1],
        :name => a[2],
        :dest => a[4] || default_dest || 'target',
        :op => a[6] || '>',
        :version => a[7] || '0',
        :use_restrict => (a[9]||'').split(/[, ]/)
      }
    end

    def self.tweak_spec(spec, new_dest)
      a = dissect_spec(spec)
      new_spec = "#{a[2]}"
      new_spec += "[#{a[4] || new_dest}]" if !a[4].nil? || new_dest != 'target'
      new_spec += "#{a[6]}#{a[7]}"
      new_spec += "{#{a[9]}}" if a[9]
      new_spec
    end

    def self.version_compare(v1, v2, op=nil)
      op = '==' if op == '='
      sv1, sv2 = v1.split('.'), v2.split('.')
      lm = [sv1.length, sv2.length].max
      sv1 += ['0'] * (lm - sv1.length)
      sv2 += ['0'] * (lm - sv2.length)
      sv1.each_index { |i|
        csv1, csv2 = sv1[i], sv2[i]
        if (Integer(sv1[i]) rescue false) && (Integer(sv2[i]) rescue false)
          csv1, csv2 = sv1[i].to_i, sv2[i].to_i
        end

        if [csv1, csv2].include? '*'
          res = 0
          break
        end

        res = csv1 <=> csv2
        if res != 0
          if op.nil?
            return res
          else
            return res.send(op, 0)
          end
        end
      }
      if op.nil?
        return 0
      else
        0.send(op, 0)
      end
    end

    def self.satisfies?(spec, package)
      spec = parse_spec(spec)
      spec[:name] == package.name && version_compare(package.version, spec[:version], spec[:op])
    end

    def self.variants(spec, strict)
      f = parse_spec(spec)
      name, dest, op, version, uses = f[:name], f[:dest], f[:op], f[:version], f[:use_restrict]

      qbuilds = []

      if @virtuals.has_key? name
        qbuilds = @virtuals[name]
      elsif @qbuilds.has_key? name
        stock = @qbuilds[name]
        allowed_versions = stock.keys.select { |v|
          flags = use_flags(stock[v], dest)
          unsatisfied = uses.select { |u|
            u[0] == ?+ && !flags.include?(u[1..-1].to_sym) ||
              u[0] == ?- && flags.include?(u[1..-1].to_sym)
          }

          if unsatisfied.any? && strict
            raise UseFlagMismatch, "#{spec} has unmet use flag dependencies #{unsatisfied.join ', '}"
          end

          current_arch = arch_for(dest)
          keywords_ok = true

          if current_arch
            global_keywords = @keywords.flags_for(stock[v]).map &:to_s
            qbuild_keywords = invoke(stock[v], dest).keywords.map &:to_s

            keywords_ok = qbuild_keywords.include?(current_arch) ||
                 (!qbuild_keywords.include?("-#{current_arch}") && global_keywords.include?(current_arch))
          end

          unsatisfied.empty? && keywords_ok && version_compare(v, version, op)
        }
        qbuilds = stock.values_at(*allowed_versions.sort.reverse)
      else
        qbuilds = []
      end

      qbuilds.map { |qbuild| invoke(qbuild, dest) }
    end

    def self.get(spec)
      qbuild = @resolver.resolved_specs[spec]
      raise Exception, "attempting to get unresolved spec #{spec}" if qbuild.nil?
      qbuild
    end

    def self.arch_for(dest)
      if dest == 'target'
        @keyword_mapping.map(Qbuild.target_arch)
      elsif dest == 'host' || dest == 'tools'
        dest
      else
        nil
      end
    end
    private_class_method :arch_for

    def self.invoke(qbuild, dest)
        @instance_cache[slug(qbuild, dest)] ||= qbuild.invoke(dest)
    end
    private_class_method :invoke
  end
end

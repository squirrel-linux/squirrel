=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'squirrel/warehouse'
require 'squirrel/builder'
require 'squirrel/package'
require 'squirrel/exceptions'
require 'fileutils'
require 'pathname'
require 'set'

module Squirrel
  class Qbuild
    STEPS     = [ :checkout, :prepare, :unpack_depends, :configure,
                  :build, :package, :install_tools, :install_custom, :finish ]
    DIRS      = %w{root cache dist tools etc tmp}
    ACTIONS   = %w{download unpack checkout prepare tune configure build install install_tools install_custom package}

    DEFAULT_DESTINATIONS = %w{tools host target}

    @@parallel_threads = 1
    @@offline = false

    @@make_conf = {}

    def self.load_config
      @@make_conf = {}

      lines = nil
      [ "make-#{target_tuple.split('-')[1]}.conf",
        "make-#{target_arch}.conf",
        "make.conf" ].each { |conf|
        file = "#{etc_dir}/#{conf}"
        lines = File.readlines(file) if File.exist?(file)
      }

      return if lines.nil?

      lines.each_index { |i|
        line = lines[i]
        cmd, comment = *line.split('#', 2)
        var, value = *cmd.split('=', 2)
        var.strip!
        raise ConfigError, "syntax error in make.conf at line #{i}" if value.nil?
        value.strip!
        @@make_conf[var] = value.gsub(/^"(.+)"$/, '\1')
      }
    end

    def self.make_conf
      @@make_conf
    end

    def initialize(&block)
      @qbuild_path = eval("__FILE__", block.binding)

      @files_dir = Pathname.new(qbuild_path).dirname.realpath.to_s
      @name = File.basename(File.dirname(qbuild_path))

      qbuild_path =~ /\/([a-z-]+)\/([a-z0-9-]+)\/\2-([a-z0-9._~+-]+)\.qbuild$/
      @category, @name, @version = $1, $2, $3

      @block = block

      Warehouse.add self
    end

    attr_reader :category

    def id
      "#{category}/#{name} #{version}"
    end

    attr_reader :qbuild_path

    def self.host_tuple
      `gcc -dumpmachine`.strip
    end

    def self.host_arch
      host_tuple.split('-')[0]
    end

    # generic parameters
    def self.logger=(logger)
      @@logger = logger
    end

    def self.logger
      @@logger
    end

    # build parameters
    def self.parallel_threads
      @@parallel_threads
    end

    def self.parallel_threads=(threads)
      @@parallel_threads = threads
    end

    def self.offline
      @@offline
    end

    def self.offline=(offline)
      @@offline = offline
    end

    # directories
    attr_reader :files_dir

    def self.root_dir
      @@root_dir
    end

    def self.root_dir=(root)
      @@root_dir = root
    end

    def self.staging_dir # not in DIRS
      "#{root_dir}/staging"
    end

    def self.host_staging_dir
      "#{root_dir}/host_staging"
    end

    def self.cache_dir
      "#{root_dir}/cache"
    end

    def self.dist_dir
      "#{root_dir}/dist"
    end

    def self.tools_dir
      "#{root_dir}/tools"
    end

    def self.etc_dir
      "#{root_dir}/etc"
    end

    def self.var_dir
      "#{root_dir}/var"
    end

    def self.tmp_dir
      "#{root_dir}/tmp"
    end

    %w{root staging cache dist tools etc}.each { |dir|
      define_method("#{dir}_dir") {
        self.class.send "#{dir}_dir"
      }
    }

    def self.prepare(path)
      DIRS.each { |dir|
        path.gsub! "${#{dir.upcase}}", send("#{dir}_dir")
      }
      path
    end

    def prepare(path)
      path = self.class.prepare(path)
      path.gsub! '${FILES}', files_dir
      path
    end

    # destination
    def self.target_tuple=(tuple)
      @@target_tuple = tuple
    end

    def self.target_tuple
      @@target_tuple
    end

    def self.target_arch
      target_tuple.split('-')[0]
    end

    # pre-start
    def self.prepare_env
      path = ENV['PATH'].split ':'
      path.unshift("#{host_staging_dir}/bin").unshift("#{host_staging_dir}/usr/bin")
      path.unshift("#{tools_dir}/bin").unshift("#{tools_dir}/usr/bin")
      path.unshift("#{tools_dir}/sbin").unshift("#{tools_dir}/usr/sbin")
      ENV['PATH'] = path.uniq.join ':'
      libpath = [ "#{tools_dir}/lib", "#{tools_dir}/usr/lib",
                  "#{staging_dir}/usr/local/lib", "#{host_staging_dir}/usr/lib" ]
      ENV['LD_LIBRARY_PATH'] = libpath.uniq.join ':'
    end

    def self.prepare_tree
      FileUtils.mkdir_p cache_dir
      FileUtils.mkdir_p dist_dir
      FileUtils.mkdir_p temp_dir
    end

    # attributes
    attr_reader :name, :version

    # the most important function
    def invoke(destination)
      Qinstance.new(self, @block, destination)
    end
  end
end

def Qbuild(&block)
  Squirrel::Qbuild.new(&block)
end

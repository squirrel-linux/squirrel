=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class Tools
      def initialize(path="#{Qbuild.var_dir}/tools.state")
        @path = path

        load
      end

      def load
        @tools = {}
        @tools_dest = {}
        File.read(@path).lines.each { |line|
          if !line.empty?
            fields = line.strip.split(/\s+/, 3)
            @tools[fields[0]] = fields[1]
            @tools_dest[fields[0]] = fields[2].split
          end
        } rescue nil
      end

      def save
        File.open(@path, "w") { |f|
          @tools.each { |name, version|
            f.puts "#{name} #{version} #{@tools_dest[name].join ' '}"
          }
        }
      end
    end
  end
end

require 'squirrel/config/common/tools'

=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class KeywordMapping
      def initialize(path="#{Qbuild.etc_dir}/keyword-map.conf")
        @archs = {}

        (File.readlines(path) rescue []).each { |line|
          line.sub! /#.*$/, ''
          line.strip!
          next if line.empty?

          keyword, *archs = *line.split
          archs.each { |arch| @archs[Regexp.new(arch)] = keyword }
        }
      end
    end
  end
end

require 'squirrel/config/common/keyword_mapping'

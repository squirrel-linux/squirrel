=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class World
      def initialize(path="#{Qbuild.var_dir}/world.state")
        @path = path

        load
      end

      def append(spec)
        @specs << spec

        save
      end

      def specs
        @specs.dup
      end

      def load
        @specs = []

        File.readlines(@path).each { |line|
          line.strip!
          @specs << line if !line.empty?
        } rescue nil
      end

      def save
        File.open(@path, 'w') { |f|
          @specs.uniq.each { |spec|
            f.puts spec
          }
        }
      end
    end
  end
end

require 'squirrel/config/common/world'

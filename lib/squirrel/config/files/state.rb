=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class State
      def initialize(path="#{Qbuild.var_dir}/build.state")
        @path = path

        load
      end

      def load
        @qbuilds = []
        @use_flags = {}
        needs_saving = false

        (File.readlines(@path) rescue []).each { |line|
          line.strip!
          next if line.empty?
          name, version, destination, *use_flags = *line.split
          qbuild = Warehouse.fetch(name, version, destination)
          if qbuild.nil?
            Qbuild.logger.log "Cannot locate qbuild #{name}[#{destination}]=#{version}"+
                              " qbuild from statefile.", :warn
            needs_saving = true
          else
            @qbuilds << qbuild
            @use_flags[qbuild] = Set.new(use_flags.map { |f| f.to_sym })
          end

          save if needs_saving
        }
      end

      def save
        File.open(@path, 'w') { |f|
          @qbuilds.each { |qb|
            f.puts "#{qb.name} #{qb.version} #{qb.destination} #{@use_flags[qb].to_a.join ' '}"
          }
        }
      end
    end
  end
end

require 'squirrel/config/common/state'

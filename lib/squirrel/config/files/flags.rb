=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010, 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class Flags
      def initialize(id)
        @path = "#{Qbuild.etc_dir}/#{id}.conf"

        load
      end

      def load
        @flags = Set[]
        @local_flags = {}

        File.readlines(@path).each { |line|
          data = line.split('#', 2)[0]
          next if data.strip.empty?
          package, *flags = data.split
          next if flags.empty?
          if package == '*'
            flags.each { |f|
              next if f[0] == ?-
              f = f[1..-1] if f[0] == '+'
              @flags << f.intern
            }
          else
            @local_flags[package] = flags
          end
        } rescue nil
        @saved_flags = @flags.clone
        @saved_local_flags = @local_flags.clone
      end

      def save
        File.open(@path, "w") { |f|
          f.puts "# Automatically generated on #{Time.now}."
          f.puts "# You may edit if you wish."
          f.puts "* #{@flags.map { |flag| flag.to_s }.join ' '}"
          @local_flags.each { |pkg, flags|
            f.puts "#{pkg} #{flags.join ' '}"
          }
        }
        Warehouse.flush
      end
    end
  end
end

require 'squirrel/config/common/flags'

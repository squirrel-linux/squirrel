=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class Flags
      attr_reader :flags, :local_flags

      def flags_for(package)
        flags = @flags.dup
        if !package.nil?
          overrides = @local_flags.select { |s,o| Warehouse.satisfies?(s, package) }.map { |s,o| o }.flatten
          overrides.each { |o|
            if o[0] == ?-
              flags.delete o[1..-1].intern
            elsif o[0] == ?+
              flags << o[1..-1].intern
            else
              flags << o.intern
            end
          }
        end
        flags
      end

      def changed?
        @flags != @saved_flags || @local_flags != @saved_local_flags
      end

      def change_affects
        global = (@flags ^ @saved_flags).map{ |f| Warehouse.uses_packages(f) }.flatten
        _changed = lambda { |f1, f2| f1.select { |k,v| v != f2[k] }.map { |k,v| k } }
        local = (lambda{|f1, f2| _changed[f1,f2] + _changed[f2,f1]}.call(@local_flags, @saved_local_flags))
        (local + global).uniq
      end
    end
  end
end

=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2011 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class Tools
      def register(qbuild)
        @tools[qbuild.name] = qbuild.version
        dests = (@tools_dest[qbuild.name] ||= [])
        dests << qbuild.destination
        dests.uniq!
        save
      end

      def built?(qbuild)
        @tools[qbuild.name] == qbuild.version && @tools_dest[qbuild.name].include?(qbuild.destination)
      end
    end
  end
end

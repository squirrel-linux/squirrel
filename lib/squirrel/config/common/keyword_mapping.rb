=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class KeywordMapping
      def map(arch)
        keywords = @archs.keys.select { |regexp| arch =~ regexp }.map { |k| @archs[k] }
        if keywords.count > 1
          raise SquirrelError.new("Multiple keywords match architecture `#{arch}': #{keywords.join ', '}.")
        elsif keywords.count == 1
          keywords.first
        else
          arch
        end
      end
    end
  end
end

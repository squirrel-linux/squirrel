=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  module Warehouse
    class State
      def append(qbuild)
        @qbuilds << qbuild if !@qbuilds.include? qbuild
        @use_flags[qbuild] = qbuild.used
        save
      end

      def qbuilds
        @qbuilds.dup
      end

      def use_flags
        @use_flags.dup
      end

      def use_updates
        @qbuilds.select { |q|
          q.used != @use_flags[q]
        }
      end
    end
  end
end

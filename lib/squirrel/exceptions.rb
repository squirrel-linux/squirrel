=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

module Squirrel
  class SquirrelError < StandardError
  end

  # Builder

  class BuilderException < SquirrelError
  end

  class StopAfterQinstallException < SquirrelError
  end

  # Qbuild

  class ConfigError < SquirrelError
  end

  class QbuildSyntaxError < SquirrelError
    def initialize(message, qbuild)
      super(message)
      @filename = qbuild.qbuild_path
    end

    attr_reader :filename
  end

  # Warehouse / Resolver

  class QbuildNotFoundError < SquirrelError
  end

  class NoSolutionError < SquirrelError
  end

  class AmbiguousReferenceError < SquirrelError
    def initialize(spec, variants)
      super "Spec #{spec} is ambiguous and may be resolved as #{variants.map{|v| v.id}.join ', '}."
      @spec, @variants = spec, variants
    end

    attr_reader :spec, :variants

    def compact_variants
      sorted = {}
      @variants.each { |v| (sorted[v.name] ||= []) << v }
      sorted.each { |k,v| v.sort! { |a,b| Warehouse.version_compare(a.version, b.version) } }
      sorted.map { |k,v| v[0] }
    end
  end

  class UseFlagMismatch < SquirrelError
  end
end

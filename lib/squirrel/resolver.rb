=begin
  Squirrel, a fast and flexible Linux distribution builder.
  Copyright (C) 2009, 2010 Peter Zotov <whitequark@whitequark.org>.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

require 'squirrel/exceptions'

module Squirrel
  class Resolver
    def initialize(specs, options={})
      @logger = Qbuild.logger
      @state = {}
      @state[0] = { :qbuilds => [], :requires => [], :specs => specs, :resolved_specs => {} }
      @selection = {}
      @cache = nil # @cache = { 'qbuild' => { 'slot' => #<Qbuild>, ... }, ... }
      @virtuals = nil # @virtuals = { 'virtual' => #<Qbuild> }
      @all_qbuilds = []
      @strict = options[:strict] || true
      @step = 0
    end

    def step
      debug "Step #{@step}."

      regenerate_cache if @cache.nil?

      state = @state[@step]
      qbuilds, requires, specs, resolved_specs = state[:qbuilds].dup, state[:requires].dup,
                                                 state[:specs].dup, state[:resolved_specs].dup
      spec = specs.shift

      # this_is_sparta!
      this_is_require = (spec[0] == ?!)
      spec = spec[1..-1] if this_is_require

      debug " Qbuilds: #{qbuilds.map{|q|q.id}.join ', '}." if qbuilds.any?
      debug " Resolving #{spec}."

      variants = Warehouse.variants(spec, @strict)
      if variants.empty?
        debug " #{spec} has no variants."
        return false
      end

      debug " #{spec} => #{variants.map{|q|q.id}.join ', '}."

      if variants.map { |q| q.name }.uniq.count > 1
        if variants.select { |q| @all_qbuilds.include? q }.empty?
          raise AmbiguousReferenceError.new(spec, variants)
        end
      end

      variants.sort! { |k2, k1| Warehouse.version_compare(k1.version, k2.version) }

      selected = @selection[@step] || 0

      valid_variants = []

      variants.each { |v|
        if lookup_cache(v) != v && !lookup_cache(v).nil?
          debug "  C #{v.id} (slot #{v.slot} occupied)"
          next
        end

        if v.provides.select { |p| cv = @virtuals[cache_key_virtual(p, v.destination)]; cv && cv != v }.any?
          debug "  P #{v.id} (virtuals #{v.provides.join ', '} conflict)"
          next
        end

        valid_variants << v

        info =  "  #{if selected < valid_variants.count then '+' else 'D' end} #{v.id}"
        info += " => #{v.build_depends.join ', '}" if v.build_depends.any?
        debug info
      }

      if valid_variants.empty?
        debug " No valid variants."
        return false
      end

      variant = nil

      if valid_variants.count == 1
        debug " One valid variant."
        variant = valid_variants[0]
      elsif selected < valid_variants.count
        debug " Selecting newest version (#{valid_variants[selected].id})."
        variant = valid_variants[selected]
        @selection[@step] ||= 0
      else
        debug " Cannot select anything."
        return false
      end

      resolved_specs[spec] = variant

      if !qbuilds.include? variant
        if !this_is_require
          qbuilds << variant
          specs += variant.build_depends.map { |v| Warehouse.tweak_spec(v, variant.destination) } - resolved_specs.keys
        else
          requires << variant
        end
        specs += variant.build_requires.map { |v| Warehouse.tweak_spec(v, variant.destination) }.map { |v| "!#{v}" }
        update_cache(variant)
      end

      @step += 1
      @selection.delete @step
      @state[@step] = { :qbuilds => qbuilds,
                        :requires => requires,
                        :specs => specs,
                        :resolved_specs => resolved_specs }
    end

    def rollback
      debug "Rolling back from step #{@step}."

      @cache = nil

      @step -= 1
      while @step >= 0 && @selection[@step].nil?
        @step -= 1
      end

      if @step < 0
        debug " No decisions left."
        false
      else
        debug " Previous decision at step #{@step}."
        true
      end
    end

    def resolved?
      @state[@step][:specs].empty?
    end

    def check_requires
      requires = @state[@step][:requires]
      requires.select { |r| r.needs_building? && !qbuilds.include?(r) }.each do |r|
        raise NoSolutionError, "Qbuild #{r.id} is not built and is not going to be built."
      end
    end

    def auto_resolve
      while !resolved?
        if !step
          if rollback
            @selection[@step] += 1
          else
            raise NoSolutionError, "Resolver is out of solutions."
          end
        end
      end

      check_requires

      debug "Solution: #{qbuilds.map{|q|q.id}.join ', '}."
      true
    end

    def qbuilds
      @state[@step][:qbuilds]
    end

    def resolved_specs
      @state[@step][:resolved_specs]
    end

    private

    def regenerate_cache
      qbuilds = @state[@step][:qbuilds]
      @cache = {}
      @virtuals = {}
      @all_qbuilds = Warehouse.state.qbuilds + qbuilds
      @all_qbuilds.uniq!

      qbuilds.each { |qb|
        update_cache(qb)
      }
      @all_qbuilds.each { |qb|
        update_virtuals(qb)
      }
    end

    def cache_key(qbuild)
      "#{qbuild.name}%#{qbuild.destination}"
    end

    def cache_key_virtual(virtual, destination)
      "#{virtual}%#{destination}"
    end

    def update_cache(qbuild)
      key = cache_key(qbuild)
      @cache[key] ||= {}
      @cache[key][qbuild.slot] = qbuild
      update_virtuals(qbuild)
      @all_qbuilds << qbuild if !@all_qbuilds.include? qbuild
    end

    def update_virtuals(qbuild)
      qbuild.provides.each { |v|
        @virtuals[cache_key_virtual(v, qbuild.destination)] = qbuild
      }
    end

    def lookup_cache(qbuild)
      key = cache_key(qbuild)
      if @cache.has_key?(key)
        @cache[key][qbuild.slot]
      else
        nil
      end
    end

    def debug(msg)
      @logger.log msg, :debug
    end
  end
end
